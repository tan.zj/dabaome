import react from '@vitejs/plugin-react';
import { nxViteTsPaths } from '@nx/vite/plugins/nx-tsconfig-paths.plugin';
import { TanStackRouterVite } from '@tanstack/router-vite-plugin';
import path from 'path';
import { createServer } from 'vite';
import { fileURLToPath } from 'url';
import inquirer from 'inquirer';

const __dirname = fileURLToPath(new URL('.', import.meta.url));

/**
 * Ideas incase decoupling of applications does not pan out
 *
 * Write a node api-based vite config server
 * - This assumes that the bundling works out of the box, the last time I tried, it did not cache
 * - Can accept as options when starting dev server
 * - Using node inquirer to form the options and pass to the node api vite create server
 *
 */

const startDevOptions = async () => {
  const inquirerOptions = await inquirer.prompt([
    {
      type: 'checkbox',
      name: 'name',
      message: 'What would you like cached?',
      choices: [
        '@fe-monorepo-poc/breadcrumbs',
        '@fe-monorepo-poc/store',
        '@fe-monorepo-poc/search',
        '@fe-monorepo-poc/digital',
        '@fe-monorepo-poc/workspace',
      ],
    },
  ]);

  const dependencies = await inquirerOptions.name;

  const viteDevServerConfig = await createServer({
    configFile: false,
    root: path.join(__dirname, '..', 'apps', 'poc'),
    server: {
      port: 4200,
      host: 'localhost',
    },

    plugins: [
      TanStackRouterVite({
        routeFileIgnorePattern: '.((css|const).ts)',
        routesDirectory: path.join(
          __dirname,
          '..',
          'apps',
          'poc',
          'src',
          'pages',
        ),
        generatedRouteTree: path.join(
          __dirname,
          '..',
          'apps',
          'poc',
          'src',
          'routeTree.gen.ts',
        ),
      }),
      react(),
      nxViteTsPaths(),
    ],

    // This force optimize listen dependencies
    // Must be resolvable import paths, cannot be globs
    // https://vitejs.dev/guide/dep-pre-bundling.html#monorepos-and-linked-dependencies
    optimizeDeps: {
      include: dependencies,
      force: true, // on dev server start, do a force bundle
    },
  });

  await viteDevServerConfig.listen();

  console.log('Welcome to the development of TSC Proof of Concepts\n');
  console.log(
    'This is still a highly work in progress project, so do bear with the inconveniences!\n',
  );

  console.log(String.raw`
__        __   _                             _          _____ ____   ____ 
\ \      / /__| | ___ ___  _ __ ___   ___   | |_ ___   |_   _/ ___| / ___|
 \ \ /\ / / _ \ |/ __/ _ \|  _   _ \ / _ \  | __/ _ \    | | \___ \| |    
  \ V  V /  __/ | (_| (_) | | | | | |  __/  | || (_) |   | |  ___) | |___ 
   \_/\_/ \___|_|\___\___/|_| |_| |_|\___|   \__\___/    |_| |____/ \____|
                                                                         
`);
  console.log('\n');
  console.log(
    '[INFO] Vite dev server started successfully at http://localhost:4200 \n',
  );
};

startDevOptions();
